package com.tat.barannik;

import java.util.*;

public class Main {

    public static void main(String args[]) {
        int algorithmId;
        int loopType;
        int n;

        try {
            algorithmId = Integer.parseInt(args[0]);
            loopType = Integer.parseInt(args[1]);
            n = Integer.parseInt(args[2]);
            switch (algorithmId) {
                case 1:
                    calcFibonacci(n, loopType);
                    break;
                case 2:
                    System.out.println(calcFactorial(n, loopType));
                    break;
                default:
                    throw new RuntimeException("Invalid value");
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    public static void calcFibonacci(int n, int loopType) {
        try{
            switch (n) {
                case 0:
                    throw new ArrayIndexOutOfBoundsException("Invalid value 'n'");
                case 1:
                    System.out.println("[0]");
                    break;
                case 2:
                    System.out.println("[0, 1]");
                    break;
                default:
                    chooseMethodFibonacci(n, loopType);
                    break;
            }
        } catch(NegativeArraySizeException e){
            e.printStackTrace();
        }
    }

    public static void chooseMethodFibonacci(int n, int loopType) {
        int[] fibonacci = new int[n];
        fibonacci[0] = 0;
        fibonacci[1] = 1;
        int i = 2;
        switch (loopType) {
            case 1:
                while (i < n) {
                    fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
                    ++i;
                }
                System.out.println(Arrays.toString(fibonacci));
                break;
            case 2:
                do {
                    fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
                    ++i;
                } while (i < n);
                System.out.println(Arrays.toString(fibonacci));
                break;
            case 3:
                for (; i < n; ++i) {
                    fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
                }
                System.out.println(Arrays.toString(fibonacci));
                break;
            default:
                throw new RuntimeException("Invalid value 'loopType'");
        }


    }

    public static int calcFactorial(int n, int loopType) {
        int result = 1;
        int c = 1;
        switch (loopType) {
            case 1:
                for (; c <= n; c++) {
                    result = result * c;
                }
                return result;
            case 2:
                while (c <= n) {
                    result = result * c;
                    c++;
                }
                return result;
            case 3:
                do {
                    result = result * c;
                    c++;
                } while (c <= n);
                return result;
            default:
                throw new RuntimeException("Invalid value");
        }
    }
}




