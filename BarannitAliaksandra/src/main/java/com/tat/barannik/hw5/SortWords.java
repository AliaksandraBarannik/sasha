package com.tat.barannik.hw5;

import java.io.InputStreamReader;
import java.util.Scanner;

public class SortWords {

    public static void main(String[] args) {

        String str = "";

        Scanner scanner = new Scanner(new InputStreamReader(System.in));
        str = scanner.nextLine();
        scanner.close();
        String text = str.replaceAll("[^a-zA-Z\\s]", "");
        String[] words = (text).trim().split("\\p{P}?[ \\t\\n\\r]+");
        sort(words);
        String group = null;
        String prev = null;
        for (String w : words) {
            String l = w.substring
                    (0, 1).toUpperCase();
            if (!w.equals(prev)) {
                prev = w;
            } else {
                continue;
            }
            if (l.equals(group)) {
                System.out.print(w + " ");
            } else {
                if (group != null) {
                    System.out.println();
                }
                group = l;
                System.out.print(l + ": " + w + " ");
            }
        }
    }

    public static void sort(String[] arr) {

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                String l1 = arr[j].substring(0, 1);
                String l2 = arr[j + 1].substring(0, 1);
                if (l1.compareToIgnoreCase(l2) > 0) {
                    String a = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = a;
                }
            }
        }
    }
}
