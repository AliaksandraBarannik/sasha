package com.tat.barannik.hw7.task2;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class MyHashSet<T> extends HashSet {
    private final Collection<T> items1;
    private final Collection<T> items2;

    public MyHashSet(Collection<T> items1, Collection<T> items2) {
        this.items1 = items1;
        this.items2 = items2;
    }

    public static void main(String[] args) {
        List<String> items1 = Arrays.asList("a", "b", "c");
        List<String> items2 = Arrays.asList("c", "d", "e");
        MyHashSet<String> set = new MyHashSet<>(items1, items2);
        System.out.println(set.union());
        System.out.println(set.complements());
        System.out.println(set.reverseComplements());
        System.out.println(set.intersection());
        System.out.println(set.symmetricDifference());
    }


    public Collection<T> union() {
        HashSet<T> set = new HashSet<>();
        set.addAll(items1);
        set.addAll(items2);
        return set;
    }

    private Collection<T> myComplements(Collection<T> items1, Collection<T> items2){
        HashSet<T> set = new HashSet<>(items1);
        set.removeAll(items2);
        return set;
    }

    public Collection<T> complements() {
        return this.myComplements(this.items1, this.items2);
    }

    public Collection<T> reverseComplements() {
        return this.myComplements(this.items2, this.items1);
    }

    public Collection<T> intersection() {
        HashSet<T> set = new HashSet<>(this.union());
        set.retainAll(items1);
        set.retainAll(items2);
        return set;
    }

    public Collection<T> symmetricDifference() {
        HashSet<T> set = new HashSet<>(this.complements());
        set.addAll(new MyHashSet<>(items2, items1).complements());
        return set;
    }
}

