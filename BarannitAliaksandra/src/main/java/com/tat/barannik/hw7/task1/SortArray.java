package com.tat.barannik.hw7.task1;

import java.util.Random;

public class SortArray {

    public static void main(String[] args) {
        int size = 10;
        int[] arr = new int[size];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100);
            System.out.print(arr[i] + " ");
        }
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if (sum(arr[j]) > sum(arr[j + 1])) {
                    int a = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = a;
                }
            }
        }
        System.out.println();
        for (int e : arr) {
            System.out.print(e + " ");
        }
    }

    public static int sum(int elem) {
        int n = elem;
        int s = 0;
        while (n > 0) {
            s += n % 10;
            n = n / 10;
        }
        return s;
    }
}

