package com.tat.barannik.hw9;

import java.io.*;
import java.sql.*;
import java.util.*;

public class TableOfEmployee implements Serializable {
    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    public static void main(String[] args) {
        TableOfEmployee tableOfEmployee = new TableOfEmployee();
        tableOfEmployee.openConnection();
        tableOfEmployee.listenConsole();
        tableOfEmployee.closeConnection();
    }

    public void openConnection() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:src/main/resources/db/db_for_m4_l3.db");
            System.out.println("Opened database successfully");
            statement = connection.createStatement();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public void closeConnection() {
        try {
            resultSet.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    public void listenConsole() {
        int id = -1;
        System.out.println("To select an operation, enter:" + "\n" + "1. ''add' and enter firstname and lastname of an employee' to add an employee," + "\n" + "2. ''delete' and enter id of an employee' to remove an employee," + "\n" + "3. 'list' to see the list of employees," + "\n" + "4. 'exit' to exit");
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String str = scanner.nextLine();
            try {
                String[] data = str.split(" ");
                String command = data[0];
                switch (command) {
                    case "add": {
                        String firstName = data[1];
                        String lastName = data[2];
                        if (firstName != null && lastName != null) {
                            String sql = "insert into Employee(firstname, lastname) values(?, ?);";
                            preparedStatement = connection.prepareStatement(sql);
                            preparedStatement.setString(1, firstName);
                            preparedStatement.setString(2, lastName);
                            preparedStatement.executeUpdate();
                            System.out.println("An employee has been added");
                            System.out.println("Command was successful, enter the following command");
                        }
                    }
                    break;
                    case "delete":
                        id = Integer.parseInt(data[1]);
                        if (id >= 0) {
                            try {
                                resultSet = statement.executeQuery("SELECT * from Employee where ID=" + id);
                                if (!resultSet.next()) {
                                    System.out.println("Cannot be deleted. The employee with this 'id' does not exist.Please, repeate a command with correct 'id'");
                                } else {
                                    preparedStatement = connection.prepareStatement("delete from Employee where ID=?;");
                                    preparedStatement.setInt(1, id);
                                    preparedStatement.executeUpdate();
                                    System.out.println("An employee has been deleted");
                                    System.out.println("Command was successful, enter the following command");
                                }
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    case "list":
                        resultSet = statement.executeQuery("select * from Employee;");
                        while (resultSet.next()) {
                            int idOfEmployee = resultSet.getInt(1);
                            String firstName = resultSet.getString(2);
                            String lastName = resultSet.getString(3);
                            System.out.println(idOfEmployee + " " + firstName + " " + lastName);
                        }
                        System.out.println("Command was successful, enter the following command");
                        break;
                    case "exit":
                        this.closeConnection();
                        System.exit(0);
                        break;
                    default:
                        System.out.println("Invalid command. Please enter a correct command. Please, repeate a command without empty space");
                        break;
                }
            } catch (Throwable t) {
                System.out.println("Please repeate a command without empty space");
            }
        }
    }
}
