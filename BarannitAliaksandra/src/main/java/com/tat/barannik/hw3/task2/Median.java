package com.tat.barannik.hw3.task2;

import java.util.Arrays;

public final class Median {
    public static void main(String[] args) {

    }

    public static float median(int[] array) {
        Arrays.sort(array);
        int middle = array.length / 2;
        if (array.length % 2 == 0) {
            int left = array[middle - 1];
            int right = array[middle];
            return (float) (left + right) / 2;
        } else {
            return (float) array[middle];
        }
    }

    public static double median(double[] array) {
        Arrays.sort(array);
        int middle = array.length / 2;
        if (array.length % 2 == 0) {
            double left = array[middle - 1];
            double right = array[middle];
            return (left + right) / 2;
        } else {
            return array[middle];
        }
    }
}