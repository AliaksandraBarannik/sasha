package com.tat.barannik.hw3.task1;

public class Card {
    private String name;
    private double balance;

    public static void main(String[] args) {

    }

    Card() {
    }

    Card(String name, double balance) {
        if (balance > 0) {
            this.name = name;
            this.balance = balance;
        } else {
            throw new RuntimeException("Account balance can not be negative");
        }
    }

    Card(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getBalance() {
        return balance;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBalance(double balance) {
        if (balance >= 0) {
            this.balance = balance;
        } else {
            throw new RuntimeException("Account balance can not be negative");
        }
    }

    double depositMoney(double depositSum) {
        if (depositSum > 0) {
            balance += depositSum;
            return balance;
        } else {
            throw new RuntimeException("Deposit amount can not be negative");
        }
    }

    void withdrawMoney(double withdrawSum) {
        if (balance > withdrawSum && withdrawSum > 0) {
            balance -= withdrawSum;
        } else {
            throw new RuntimeException("The withdrawal amount exceeds the balance on the account or negative");
        }
    }

    double getBalanceAfterConversation(double conversionRate) {
        if (conversionRate > 0) {
            return balance * conversionRate;
        }
        throw new RuntimeException("Enter currency of conversion: value must be greater than zero ");
    }
}

