package com.tat.barannik.hw8;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Set;

public class TableOfEmployee implements Serializable {
    private int autoId = 1;
    private LinkedList<Employee> table = new LinkedList<>();

    public static void main(String[] args) {
        TableOfEmployee tableOfEmployee = new TableOfEmployee();
        tableOfEmployee.listenConsole();
    }

    private int getNextAutoId() {
        Set<Integer> ids = new HashSet<>();
        for (Employee e : table) {
            ids.add(e.getId());
        }
        while (ids.contains(autoId) && autoId < Integer.MAX_VALUE) {
            ++autoId;
        }
        if (autoId == Integer.MAX_VALUE) {
            System.out.println("Table doesn't have enough space for a new record.");
            return -1;
        }
        return autoId;
    }

    public void listenConsole() {
        Employee employee = null;
        int id = -1;
        System.out.println("To select an operation, enter:" + "\n" + "1. ''add' and enter firstname and lastname of an employee' to add an employee," + "\n" + "2. ''delete' and enter id of an employee' to remove an employee," + "\n" + "3. 'list' to see the list of employees," + "\n" + "4. 'save' to save the table," + "\n" + "5. 'load' to load the table and show the list of employees," + "\n" + "6. 'exit' to exit.");
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String str = scanner.nextLine();
            try {
                String[] data = str.split(" ");
                String command = data[0];
                switch (command) {
                    case "load":
                        openTable();
                        System.out.println("Command was successful, enter the following command");
                        break;
                    case "add":
                        id = getNextAutoId();
                        String firstName = data[1];
                        String lastName = data[2];
                        if (id >= 0 && firstName != null && lastName != null) {
                            employee = new Employee(id, firstName, lastName);
                            if (!table.contains(employee)) {
                                table.add(employee);
                                System.out.println("An employee has been added: " + employee.toString());
                                System.out.println("Command was successful, enter the following command");
                            } else {
                                System.out.println("An employee with this ID is already existed.");
                            }
                        }
                        break;
                    case "delete":
                        id = Integer.parseInt(data[1]);
                        if (id >= 0) {
                            Iterator<Employee> it = table.iterator();
                            boolean deleted = false;
                            while (it.hasNext()) {
                                if (it.next().getId() == id) {
                                    it.remove();
                                    System.out.println("An employee has been deleted");
                                    System.out.println("Command was successful, enter the following command");
                                    deleted = true;
                                    break;
                                }
                            }
                            if (!deleted) {
                                System.out.println("Cannot be deleted. The employee with this 'id' does not exist.Please, repeate a command with correct 'id'");
                            }
                        }
                        break;
                    case "list":
                        showEmployee();
                        System.out.println("Command was successful, enter the following command");
                        break;
                    case "save":
                        saveTable();
                        System.out.println("The table has been saved");
                        System.out.println("Command was successful, enter the following command");
                        break;
                    case "exit":
                        System.exit(0);
                        break;
                    default:
                        System.out.println("Invalid command. Please, enter a correct command without empty space");
                        break;
                }
            } catch (Throwable t) {
                System.out.println("Please repeate a command without empty space");
            }
        }
    }

    public void showEmployee() {
        for (Employee employee : table) {
            System.out.println(employee.toString());
        }
    }

    public void saveTable() {
        try {
            FileOutputStream file = new FileOutputStream("savetable.ser");
            ObjectOutputStream saving = new ObjectOutputStream(file);
            saving.writeObject(table);
            saving.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void openTable() {
        try {
            File file = new File("savetable.ser");
            if (!file.exists()) return;
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream opening = new ObjectInputStream(fis);
            table = new LinkedList<>((LinkedList<Employee>) opening.readObject());
            for (Employee employee : table) {
                System.out.println(employee.toString());
            }
            opening.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
