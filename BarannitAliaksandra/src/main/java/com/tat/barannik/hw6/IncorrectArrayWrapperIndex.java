package com.tat.barannik.hw6;

public class IncorrectArrayWrapperIndex extends RuntimeException {

    public IncorrectArrayWrapperIndex(String message) {
        super(message);
    }
}
