package com.tat.barannik.hw6;

public class ArrayWrapper<T> {

    private T[] values;


    public ArrayWrapper(T[] values) {
        this.values = values;
    }

    public T get(int i) {
        int n = getIndex(i);
        return this.values[n];
    }

    private int getIndex(int i) {
        if (i > 0 && i <= this.values.length) {
            return (i - 1);
        } else {
            throw new IncorrectArrayWrapperIndex("Out of bound index.");
        }
    }

    public boolean replace(int i, T replaceValue) {
        if (replaceValue == null) {
            return false;
        }

        int n = this.getIndex(i);

        if (replaceValue instanceof String && ((String) replaceValue).length() <= ((String) this.values[n]).length()) {
            return false;
        }

        if (replaceValue instanceof Integer && ((Integer) replaceValue) <= ((Integer) this.values[n])) {
            return false;
        }

        this.values[i - 1] = replaceValue;
        return true;
    }
}