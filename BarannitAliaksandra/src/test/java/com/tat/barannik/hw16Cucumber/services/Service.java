package com.tat.barannik.hw16Cucumber.services;

import com.tat.barannik.hw16Cucumber.pages.*;

public class Service {

    BasePage basePage;
    BasketPage basketPage;
    HomePage homePage;
    ProductPage productPage;
    ResultPage resultPage;

    public void open() {
        basePage = new BasePage();
        homePage = basePage.open();
    }

    public void searchProduct(String product) {
        homePage.closePopUpWindowOnHomePage();
        homePage.inputLoginField(product);
        resultPage = homePage.clickSearchButton();
    }

    public String getTextFromFirstProduct() {
        return resultPage.getTextFromFirstProduct();
    }

    public void clickOnFirstProduct() {
        productPage = resultPage.clickOnFirstProduct();
    }

    public void addProductToBasketAndOpenBasket() {
        basketPage = productPage.addProductToBasketAndOpenBasket();
    }

    public String getTextOfProductInBasket() {
        return basketPage.getTextOfProductInBasket();
    }
}
