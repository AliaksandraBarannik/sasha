package com.tat.barannik.hw16Cucumber;

import com.tat.barannik.hw16Cucumber.Driver.Driver;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/featuresForHw16",
        strict = true,
        plugin = {
                "pretty", "json:target/Cucumber.json",
                "html:target/cucumber-html-report",
        },
        glue = "com.tat.barannik.hw16Cucumber.steps",
        tags = "@ebay"
)

public class RunnerTest {
    @AfterClass
    public static void closeDriver() {
        Driver.tearDown();
    }
}
