package com.tat.barannik.hw16Cucumber.steps;

import com.tat.barannik.hw16Cucumber.services.Service;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class EbayCheckProductInBasketDef {

    Service service;

    @Given("^I opened Ebay home page$")
    public void iOpenedEbayHomePage() {
        service = new Service();
        service.open();
    }

    @And("^I searched the product \"([^\"]*)\"$")
    public void iSearchedTheProduct(String product) {
        service.searchProduct(product);
    }

    @And("^I clicked on the first product$")
    public void iClickedOnTheFirstProduct() {
        service.clickOnFirstProduct();
    }

    @When("^I added the product to the basket and opened the basket$")
    public void iAddedTheProductToTheBasketAndOpenedTheBasket() {
        service.addProductToBasketAndOpenBasket();
    }

    @Then("^I checked the product \"([^\"]*)\" was in the basket$")
    public void iCheckedTheProductWasInTheBasket(String expectPhrase) {
        Assert.assertTrue(service.getTextOfProductInBasket().toUpperCase().contains(expectPhrase.toUpperCase()));
    }
}
