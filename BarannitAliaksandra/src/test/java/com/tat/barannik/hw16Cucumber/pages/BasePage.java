package com.tat.barannik.hw16Cucumber.pages;

import com.tat.barannik.hw16Cucumber.Driver.Driver;
import org.openqa.selenium.WebDriver;

public class BasePage {
    private final String WEBSITE_ADDRESS = "https://ebay.com/";

    protected WebDriver driver;

    public BasePage() {
        this.driver = Driver.getInstance();
    }

    public HomePage open() {
        driver.get(WEBSITE_ADDRESS);
        return new HomePage();
    }
}
