package com.tat.barannik.hw16Cucumber.steps;

import com.tat.barannik.hw16Cucumber.services.Service;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class EbaySearchDef {

    Service service;

    @Given("^I opened Ebay page$")
    public void iOpenedEbayPage() {
        service = new Service();
        service.open();
    }

    @When("^I search the requared product \"([^\"]*)\"$")
    public void iSearchTheProduct(String product) {
        service.searchProduct(product);
    }

    @Then("^the product \"([^\"]*)\" should be the first in the Search Result grid$")
    public void theProductShouldBeTheFirstInTheSearchResultGrid(String expectPhrase) {
        String textOfProduct = service.getTextFromFirstProduct();
        Assert.assertTrue(textOfProduct.toUpperCase().contains(expectPhrase.toUpperCase()));
    }
}
