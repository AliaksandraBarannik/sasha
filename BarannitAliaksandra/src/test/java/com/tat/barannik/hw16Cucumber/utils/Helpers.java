package com.tat.barannik.hw16Cucumber.utils;

import com.tat.barannik.hw16Cucumber.Driver.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Helpers {
    public static void waitForElementVisibility(By locator) {
        new WebDriverWait(Driver.getInstance(), 30).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static Boolean isElementPresent(By element) {
        return Driver.getInstance().findElements(element).size() > 0;
    }
}
