package com.tat.barannik.hw16Cucumber.pages;

import com.tat.barannik.hw16Cucumber.utils.Helpers;
import org.openqa.selenium.By;

public class HomePage extends BasePage {
    private By searchField = By.cssSelector("#gh-ac");
    private By searchButton = By.cssSelector("#gh-btn");
    private By popUpWindowCloseButton = By.cssSelector("#HomepageOverlay16162ModalClose > img");

    public HomePage() {
        super();
    }

    public void closePopUpWindowOnHomePage() {
        if (Helpers.isElementPresent(popUpWindowCloseButton)) {
            driver.findElement(popUpWindowCloseButton).click();
        }
    }

    public void inputLoginField(String product) {
        Helpers.waitForElementVisibility(searchField);
        driver.findElement(searchField).clear();
        driver.findElement(searchField).sendKeys(product);
    }

    public ResultPage clickSearchButton() {
        Helpers.waitForElementVisibility(searchButton);
        driver.findElement(searchButton).click();
        return new ResultPage();
    }
}
