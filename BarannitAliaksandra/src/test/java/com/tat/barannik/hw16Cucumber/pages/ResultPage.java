package com.tat.barannik.hw16Cucumber.pages;

import com.tat.barannik.hw16Cucumber.utils.Helpers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ResultPage extends BasePage {
    private final By listOfProducts = By.cssSelector("#srp-river-results > ul");
    private final By listOfProductElements = By.xpath("//a[contains(@class, 's-item__link')]");

    public ProductPage clickOnFirstProduct() {
        Helpers.waitForElementVisibility(listOfProductElements);
        WebElement firstProduct = driver.findElements(listOfProductElements).get(0);
        firstProduct.click();
        return new ProductPage();
    }

    public String getTextFromFirstProduct() {
        Helpers.waitForElementVisibility(listOfProducts);
        String textOfProduct = driver.findElements(listOfProductElements).get(0).getText();
        return textOfProduct;
    }

    public ResultPage() {
        super();
    }
}
