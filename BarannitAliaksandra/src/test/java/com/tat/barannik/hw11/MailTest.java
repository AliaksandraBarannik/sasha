package com.tat.barannik.hw11;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MailTest extends BaseTest {

    private final By fieldEMail = By.id("mailbox:login");
    private final By fieldPassword = By.xpath("//input[@id='mailbox:password']");
    private final By singIn = By.cssSelector("input[class='o-control']");
    private final By showPersonalData = By.id("PH_user-email");
    private final By composeButton = By.cssSelector("div[id='LEGO'] a[data-name='compose']");
    private final By fieldReceiver = By.cssSelector("textarea[data-original-name='To']");
    private final By fieldSubject = By.cssSelector("input[class='b-input']");
    private final By fieldTextOfMessage = By.id("tinymce");
    private final By frame = By.cssSelector("iframe[title='{#aria.rich_text_area}']");
    private final By saveDraftButton = By.xpath("//div[@data-name='saveDraft']");
    private final By draftsButton = By.xpath("//div[@data-id='500001']//a");
    private final By actualHeaderOfAMessage = By.xpath("//div[contains(@class, 'b-datalist_letters_to')]//div[@class='b-datalist__item__subj']");
    private final By actualTextOfAMessage = By.xpath("//div[contains(@class, 'b-datalist_letters_to')]//div[@class='b-datalist__item__subj']");
    private final By sentButton = By.xpath("//div[@data-id='500000']//a");
    private final By sendedMessage = By.xpath("//*[@class='b-datalist__item__addr' and text()='moyarabotasasha@mail.ru']");
    private final By singOutButton = By.cssSelector("a[id='PH_logoutLink']");
    private final By actualReceiverOfAMessage = By.xpath("//*[@class='b-datalist__item__addr' and text()='moyarabotasasha@mail.ru']");
    private final By choiceEmail = By.cssSelector("a[data-subject='test']");
    private final By sendButton = By.xpath("//*[@class='b-toolbar__btn__text' and text()='Send']");
    private String createdMessageLocator = "//div[contains(@data-id, '%s')][@class='b-datalist__item js-datalist-item']";
    private String createdMessageLocatorForGetAttribute = "//div[contains(@data-id, '%s')][@class='b-datalist__item js-datalist-item']//a";
    String numberOfEmailAfterSendEmail;
    String numberOfEmailBeforeSendEmail;
    String numberOfAddedEmail;

    @Test
    public void testLogin() {
        driver.get(WEBSITE_ADDRESS);
        waitForElementVisibility(fieldEMail);
        driver.findElement(fieldEMail).clear();
        waitForElementVisibility(fieldPassword);
        driver.findElement(fieldPassword).clear();
        driver.findElement(fieldEMail).sendKeys(E_MAIL);
        driver.findElement(fieldPassword).sendKeys(PASSWORD);
        driver.findElement(singIn).click();
        waitForElementVisibility(showPersonalData);
        String loginComplete = driver.findElement(showPersonalData).getText();
        Assert.assertEquals(loginComplete, E_MAIL + "@mail.ru");
        waitForElementVisibility(singOutButton);
        driver.findElement(singOutButton).click();
    }

    @Test
    public void testCreateMailAndSaveAsADraft() {
        driver.get(WEBSITE_ADDRESS);
        waitForElementVisibility(fieldEMail);
        driver.findElement(fieldEMail).clear();
        waitForElementVisibility(fieldPassword);
        driver.findElement(fieldPassword).clear();
        waitForElementVisibility(fieldEMail);
        driver.findElement(fieldEMail).sendKeys(E_MAIL);
        waitForElementVisibility(fieldPassword);
        driver.findElement(fieldPassword).sendKeys(PASSWORD);
        driver.findElement(singIn).click();
        driver.findElement(composeButton).click();
        driver.findElement(fieldReceiver).sendKeys(receiverEmail);
        driver.findElement(fieldSubject).sendKeys(headerOfAMessage);
        driver.switchTo().frame(driver.findElement(frame));
        driver.findElement(fieldTextOfMessage).sendKeys(textOfAMessage);
        driver.switchTo().defaultContent();
        driver.findElement(saveDraftButton).click();
        String currentURL = driver.getCurrentUrl();
        String idOfEMail = currentURL.replaceAll("\\D+", "").substring(0, 8);
        waitForElementVisibility(draftsButton);
        driver.findElement(draftsButton).click();
        String createdMessageLocatorWithId = String.format(createdMessageLocator, idOfEMail);
        Assert.assertTrue(elementIsExists(createdMessageLocatorWithId));
        String createdMessageLocatorWithIdForGettingAttribute = String.format(createdMessageLocatorForGetAttribute, idOfEMail);
        waitForElementVisibility(By.xpath(createdMessageLocatorWithIdForGettingAttribute));
        String actualHeaderOfAMessage = driver.findElement(By.xpath(createdMessageLocatorWithIdForGettingAttribute)).getAttribute("data-subject");
        Assert.assertTrue(actualHeaderOfAMessage.contains(headerOfAMessage));
        driver.findElement(singOutButton).click();
    }

    @Test
    public void testSendMail() {
        driver.get(WEBSITE_ADDRESS);
        waitForElementVisibility(fieldEMail);
        driver.findElement(fieldEMail).clear();
        waitForElementVisibility(fieldPassword);
        driver.findElement(fieldPassword).clear();
        waitForElementVisibility(fieldEMail);
        driver.findElement(fieldEMail).sendKeys(E_MAIL);
        waitForElementVisibility(fieldPassword);
        driver.findElement(fieldPassword).sendKeys(PASSWORD);
        driver.findElement(singIn).click();
        waitForElementToBeClickable(sentButton);
        driver.findElement(sentButton).click();
        waitForElementToBeClickable(sentButton);
        numberOfEmailBeforeSendEmail = driver.findElement(sentButton).getAttribute("data-title").replaceAll("\\D+", "");
        driver.findElement(composeButton).click();
        driver.findElement(fieldReceiver).sendKeys(receiverEmail);
        driver.findElement(fieldSubject).sendKeys(headerOfAMessage);
        driver.switchTo().frame(driver.findElement(frame));
        driver.findElement(fieldTextOfMessage).sendKeys(textOfAMessage);
        driver.switchTo().defaultContent();
        driver.findElement(saveDraftButton).click();
        String currentURL = driver.getCurrentUrl();
        String idOfEMail = currentURL.replaceAll("\\D+", "").substring(0, 8);
        waitForElementToBeClickable(draftsButton);
        driver.findElement(draftsButton).click();
        String createdMessageLocatorWithId = String.format(createdMessageLocator, idOfEMail);
        waitForElementVisibility(By.xpath(createdMessageLocatorWithId));
        driver.findElement(By.xpath(createdMessageLocatorWithId)).click();
        waitForElementToBeClickable(sendButton);
        driver.findElement(sendButton).click();
        waitForElementToBeClickable(draftsButton);
        driver.findElement(draftsButton).click();
        Assert.assertFalse(elementIsNotPresent(createdMessageLocatorWithId));
        waitForElementToBeClickable(sentButton);
        driver.findElement(sentButton).click();
        waitForElementToBeClickable(sentButton);
        numberOfEmailAfterSendEmail = driver.findElement(sentButton).getAttribute("data-title").replaceAll("\\D+", "");
        Assert.assertEquals(addedEMailSent(), "1");
        driver.findElement(singOutButton).click();
    }

    @Test
    public void testLogOut() {
        driver.get(WEBSITE_ADDRESS);
        waitForElementVisibility(fieldEMail);
        driver.findElement(fieldEMail).clear();
        waitForElementVisibility(fieldPassword);
        driver.findElement(fieldPassword).clear();
        driver.findElement(fieldEMail).sendKeys(E_MAIL);
        driver.findElement(fieldPassword).sendKeys(PASSWORD);
        driver.findElement(singIn).click();
        waitForElementVisibility(singOutButton);
        driver.findElement(singOutButton).click();
        Assert.assertTrue(isLoginFormDisplayed());
    }

    public boolean isLoginFormDisplayed() {
        waitForElementVisibility(fieldEMail);
        waitForElementVisibility(fieldPassword);
        return true;
    }

    public Boolean elementIsExists(String element) {
        driver.findElement(By.xpath(element));
        return true;
    }

    public Boolean elementIsNotPresent(String element) {
        Boolean elementCondition = false;
        try {
            elementCondition = driver.findElement(By.xpath(element)).isDisplayed();
        } catch (NoSuchElementException e) {
            return elementCondition;
        }
        return elementCondition;
    }

    public String addedEMailSent() {
        int numberBeforeSend = Integer.parseInt(numberOfEmailBeforeSendEmail);
        int numberAfterSend = Integer.parseInt(numberOfEmailAfterSendEmail);
        int addedEmail = numberAfterSend - numberBeforeSend;
        numberOfAddedEmail = Integer.toString(addedEmail);
        return numberOfAddedEmail;
    }
}
