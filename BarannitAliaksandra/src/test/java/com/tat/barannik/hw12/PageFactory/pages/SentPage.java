package com.tat.barannik.hw12.PageFactory.pages;

import org.openqa.selenium.WebDriver;

public class SentPage extends BasePage {

    public SentPage(WebDriver driver) {
        super(driver);
    }

    public String getCurrentNumberOfSentEmails() throws InterruptedException {
        // waitForElementToBeClickable(sentButton);
        Thread.sleep(10000);
        return sentButton.getAttribute("data-title").replaceAll("\\D+", "");
    }

    public String addedEMailSent(String numberOfEmailBeforeSendEmail, String numberOfEmailAfterSendEmail) {
        int numberBeforeSend = Integer.parseInt(numberOfEmailBeforeSendEmail);
        int numberAfterSend = Integer.parseInt(numberOfEmailAfterSendEmail);
        int addedEmail = numberAfterSend - numberBeforeSend;
        String numberOfAddedEmail = Integer.toString(addedEmail);
        return numberOfAddedEmail;
    }
}