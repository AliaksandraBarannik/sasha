package com.tat.barannik.hw12.PageFactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class InboxPage extends BasePage {
    @FindBy(xpath = "//div[@data-name='saveDraft']")
    protected WebElement saveDraftButton;

    @FindBy(css = "div[id='LEGO'] a[data-name='compose']")
    private WebElement writeEmailButton;

    @FindBy(css = "textarea[data-original-name='To']")
    private WebElement receiverField;

    @FindBy(css = "input[class='b-input']")
    private WebElement headerField;

    @FindBy(id = "tinymce")
    private WebElement inputTextOfEmailField;

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public void clickWriteEmailButton() {
        waitForElementToBeClickable(writeEmailButton);
        writeEmailButton.click();
    }

    public void inputReceiver(String receiverEmail) {
        receiverField.sendKeys(receiverEmail);
    }

    public void inputHeader(String headerOfAMessage) {
        headerField.sendKeys(headerOfAMessage);
    }

    public void inputTextEmail(String textOfAMessage) {
        inputTextOfEmailField.sendKeys(textOfAMessage);
    }

    public void saveDraft() {
        waitForElementToBeClickable(saveDraftButton);
        saveDraftButton.click();
    }
}
