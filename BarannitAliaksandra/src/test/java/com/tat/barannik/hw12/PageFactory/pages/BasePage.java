package com.tat.barannik.hw12.PageFactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    @FindBy(xpath = "//div[@id='PH_authView']//a[@id='PH_logoutLink']")
    protected WebElement logOutButton;

    @FindBy(xpath = "//*[@class='b-nav__item__text' and text()='Drafts']")
    protected WebElement openDraftButton;

    @FindBy(xpath = "//div[@class='w-x-ph__auth__dropdown__inner']//i[@id='PH_user-email']")
    public WebElement confirmationSingIn;

    @FindBy(xpath = "//*[@class='b-nav__item__text' and text()='Sent']")
    protected WebElement sentButton;

    @FindBy(css = "iframe[title='{#aria.rich_text_area}']")
    protected WebElement iframe;

    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void waitForElementVisibility(WebElement webElement) {
        new WebDriverWait(getDriver(), 30).until(ExpectedConditions.visibilityOf(webElement));
    }

    public void waitForElementToBeClickable(WebElement webElement) {
        new WebDriverWait(getDriver(), 30).until(ExpectedConditions.elementToBeClickable(webElement));
    }

    public boolean isElementDisplayed(WebElement fieldEMail) {
        if (fieldEMail.isDisplayed()) {
            return true;
        } else {
            return false;
        }
    }

    public LoginPage singOut() {
        waitForElementToBeClickable(logOutButton);
        logOutButton.click();
        return new LoginPage((driver));
    }

    public DraftsPage openDrafts() throws InterruptedException {
        Thread.sleep(5000);
        waitForElementToBeClickable(openDraftButton);
        openDraftButton.click();
        return new DraftsPage(driver);
    }

    public void rediscoverDrafts() throws InterruptedException {
        Thread.sleep(5000);
        waitForElementToBeClickable(openDraftButton);
        openDraftButton.click();
    }

    public SentPage openSent() {
        sentButton.click();
        return new SentPage(driver);
    }

    public void switchToFrame() {
        driver.switchTo().frame(iframe);
    }

    public void returnFromFrame() {
        driver.switchTo().defaultContent();
    }
}
