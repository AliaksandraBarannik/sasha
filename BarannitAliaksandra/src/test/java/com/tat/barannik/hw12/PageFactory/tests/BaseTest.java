package com.tat.barannik.hw12.PageFactory.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    public final String CHROME_PATH = "src/test/resources/chromedriver.exe";
    protected final String WEBSITE_ADDRESS = "https://mail.ru/";
    protected final String eMail = "testnghw";
    protected final String password = "1qaz2wsx";

    public WebDriver getDriver() {
        return driver;
    }

    protected WebDriver driver;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", CHROME_PATH);
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(WEBSITE_ADDRESS);
    }

    @AfterClass
    public void tearDoun() {
        driver.quit();
    }
}