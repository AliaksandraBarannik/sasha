package com.tat.barannik.hw12.PageObject.tests;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    public final String CHROME_PATH = "src/test/resources/chromedriver.exe";
    protected final String WEBSITE_ADDRESS = "https://mail.ru/";
    protected final String eMail = "testnghw";
    protected final String password = "1qaz2wsx";
    protected WebDriver driver;

    public WebDriver getDriver() {
        return driver;
    }

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", CHROME_PATH);
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(WEBSITE_ADDRESS);
    }

    @AfterClass
    public void tearDoun() {
        driver.quit();
    }

    public void waitForElementVisibility(By locator) {
        new WebDriverWait(driver, 50).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForElementToBeClickable(By locator) {
        new WebDriverWait(driver, 50).until(ExpectedConditions.elementToBeClickable(locator));
    }

}