package com.tat.barannik.hw12.PageObject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {

    private final By loginField = By.cssSelector("input[id='mailbox:login']");
    private final By passwordField = By.xpath("//input[@id='mailbox:password']");
    private final By singInButton = By.cssSelector("input[class='o-control']");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void inputLoginField(String eMail) {
        waitForElementVisibility(loginField);
        driver.findElement(loginField).clear();
        driver.findElement(loginField).sendKeys(eMail);
    }

    public void inputPasswordField(String password) {
        waitForElementVisibility(passwordField);
        driver.findElement(passwordField).clear();
        driver.findElement(passwordField).sendKeys(password);
    }

    public InboxPage clickSingInButton() {
        driver.findElement(singInButton).click();
        return new InboxPage(driver);
    }

    public Boolean checkIsLoginFormDisplated() {
        return isElementDisplayed(loginField);
    }
}
