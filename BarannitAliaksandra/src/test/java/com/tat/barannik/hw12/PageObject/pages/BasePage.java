package com.tat.barannik.hw12.PageObject.pages;

import com.tat.barannik.hw12.PageObject.pages.DraftsPage;
import com.tat.barannik.hw12.PageObject.pages.LoginPage;
import com.tat.barannik.hw12.PageObject.pages.SentPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    protected final By logOutButton = By.cssSelector("a[id='PH_logoutLink']");
    protected final By iframe = By.cssSelector("iframe[title='{#aria.rich_text_area}']");
    protected final By openDraftButton = By.xpath("//*[@class='b-nav__item__text' and text()='Drafts']");
    protected final By sentButton = By.linkText("Sent");
    public final By confirmationSingIn = By.id("PH_user-email");

    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public void waitForElementVisibility(By locator) {
        new WebDriverWait(driver, 50).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForElementToBeClickable(By locator) {
        new WebDriverWait(driver, 50).until(ExpectedConditions.elementToBeClickable(locator));
    }

    public boolean isElementDisplayed(By fieldEMail) {
        if (driver.findElement(fieldEMail).isDisplayed()) {
            return true;
        } else {
            return false;
        }
    }

    public LoginPage singOut() {
        waitForElementToBeClickable(logOutButton);
        driver.findElement(logOutButton).click();
        return new LoginPage((driver));
    }

    public DraftsPage openDrafts() throws InterruptedException {
        waitForElementToBeClickable(openDraftButton);
        Thread.sleep(5000);
        driver.findElement(openDraftButton).click();
        return new DraftsPage(driver);
    }

    public void rediscoverDrafts() throws InterruptedException {
        Thread.sleep(5000);
        waitForElementToBeClickable(openDraftButton);
        driver.findElement(openDraftButton).click();
    }

    public SentPage openSent() {
        driver.findElement(sentButton).click();
        return new SentPage(driver);
    }

    public Boolean isElementPresent(By element) {
        return driver.findElements(element).size() > 0;
    }

    public void switchToFrame() {
        driver.switchTo().frame(driver.findElement(iframe));
    }

    public void returnFromFrame() {
        driver.switchTo().defaultContent();
    }

//FluentWait wait = new WebDriverWait(driver, 20).ignoring( StaleElementReferenceException.class).ignoring(NoSuchElementException.class);


    /*public WebElement waitForElementVisibility(By locator) {
       return (WebElement) wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public WebElement waitForElementToBeClickable(By locator) {
        return (WebElement) wait.until(ExpectedConditions.elementToBeClickable(locator));
    }*/
}
