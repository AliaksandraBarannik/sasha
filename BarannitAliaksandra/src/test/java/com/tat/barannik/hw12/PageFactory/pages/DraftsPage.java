package com.tat.barannik.hw12.PageFactory.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DraftsPage extends BasePage {
    @FindBy(xpath = "//*[@class='b-toolbar__btn__text' and text()='Send']")
    protected WebElement sendMailButton;

    private String createdMessageLocatorForGetAttribute = "//div[contains(@data-id, '%s')][@class='b-datalist__item js-datalist-item']//a";

    public DraftsPage(WebDriver driver) {
        super(driver);
    }

    public void openDraftMail(String idOfEmail) throws InterruptedException {
        Thread.sleep(5000);
        waitForElementVisibility(creatMessageLocatorWithIdForGettingAttribute(idOfEmail));
        creatMessageLocatorWithIdForGettingAttribute(idOfEmail).click();
    }

    public void sendMail() {
        waitForElementToBeClickable(sendMailButton);
        sendMailButton.click();
    }

    public WebElement creatMessageLocatorWithIdForGettingAttribute(String idOfEMail) {
        WebElement createdMessageLocatorWithIdForGettingAttribute = driver.findElement(By.xpath(String.format(createdMessageLocatorForGetAttribute, idOfEMail)));
        return createdMessageLocatorWithIdForGettingAttribute;
    }

    public String findActualHeaderOfAMessage(String idofEmail) {
        waitForElementVisibility(creatMessageLocatorWithIdForGettingAttribute(idofEmail));
        String actualHeaderOfAMessage = creatMessageLocatorWithIdForGettingAttribute(idofEmail).getAttribute("data-subject");
        return actualHeaderOfAMessage;
    }
}
