package com.tat.barannik.hw12.PageObject.pages;

import org.openqa.selenium.WebDriver;

public class SentPage extends BasePage {

    public SentPage(WebDriver driver) {
        super(driver);
    }

    public String getCurrentNumberOfSentEmails() {
        waitForElementToBeClickable(sentButton);
        return driver.findElement(sentButton).getAttribute("data-title").replaceAll("\\D+", "");
    }

    public String addedEMailSent(String numberOfEmailBeforeSendEmail, String numberOfEmailAfterSendEmail) {
        int numberBeforeSend = Integer.parseInt(numberOfEmailBeforeSendEmail);
        int numberAfterSend = Integer.parseInt(numberOfEmailAfterSendEmail);
        int addedEmail = numberAfterSend - numberBeforeSend;
        String numberOfAddedEmail = Integer.toString(addedEmail);
        return numberOfAddedEmail;
    }
}
