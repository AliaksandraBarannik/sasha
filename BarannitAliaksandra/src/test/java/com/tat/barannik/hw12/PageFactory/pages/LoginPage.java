package com.tat.barannik.hw12.PageFactory.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

    @FindBy(css = "input[id='mailbox:login']")
    private WebElement loginField;

    @FindBy(xpath = "//input[@id='mailbox:password']")
    private WebElement passwordField;

    @FindBy(css = "input[class='o-control']")
    private WebElement singInButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void inputLoginField(String eMail) {
        waitForElementVisibility(loginField);
        loginField.clear();
        loginField.sendKeys(eMail);
    }

    public void inputPasswordField(String password) {
        waitForElementVisibility(passwordField);
        passwordField.clear();
        passwordField.sendKeys(password);
    }

    public InboxPage clickSingInButton() {
        singInButton.click();
        return new InboxPage(driver);
    }

    public Boolean checkIsLoginFormDisplated() {
        return isElementDisplayed(loginField);
    }
}
