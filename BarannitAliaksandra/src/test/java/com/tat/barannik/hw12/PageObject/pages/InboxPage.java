package com.tat.barannik.hw12.PageObject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InboxPage extends BasePage {
    private final By writeEmailButton = By.cssSelector("div[id='LEGO'] a[data-name='compose']");
    private final By receiverField = By.cssSelector("textarea[data-original-name='To']");
    private final By headerField = By.cssSelector("input[class='b-input']");
    private final By saveDraftButton = By.xpath("//div[@data-name='saveDraft']");
    private final By inputTextOfEmailField = By.id("tinymce");

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public void clickWriteEmailButton() {
        waitForElementToBeClickable(writeEmailButton);
        driver.findElement(writeEmailButton).click();
    }

    public void inputReceiver(String receiverEmail) {
        driver.findElement(receiverField).sendKeys(receiverEmail);
    }

    public void inputHeader(String headerOfAMessage) {
        driver.findElement(headerField).sendKeys(headerOfAMessage);
    }

    public void inputTextEmail(String textOfAMessage) {
        driver.findElement(inputTextOfEmailField).sendKeys(textOfAMessage);
    }

    public void saveDraft() {
        waitForElementToBeClickable(saveDraftButton);
        driver.findElement(saveDraftButton).click();
    }
}
