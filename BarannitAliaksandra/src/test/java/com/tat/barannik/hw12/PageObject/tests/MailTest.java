package com.tat.barannik.hw12.PageObject.tests;

import com.tat.barannik.hw12.PageObject.pages.*;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class MailTest extends BaseTest {
    private String receiverEmail = "moyarabotasasha@mail.ru";
    private String headerOfAMessage = "test";
    private String textOfAMessage = "Hello world!";

    InboxPage inboxPage;
    DraftsPage draftPage;
    SentPage sentPage;
    LoginPage loginPage;

    @BeforeMethod
    public void login() {
        loginPage = new LoginPage(driver);
        loginPage.inputLoginField(eMail);
        loginPage.inputPasswordField(password);
        inboxPage = loginPage.clickSingInButton();
    }

    @Test
    public void testLogin() {
        Assert.assertTrue(inboxPage.isElementDisplayed(inboxPage.confirmationSingIn));
        inboxPage.singOut();
    }

    @Test
    public void testSaveMailAsDraft() throws InterruptedException {
        inboxPage.clickWriteEmailButton();
        inboxPage.inputReceiver(receiverEmail);
        inboxPage.inputHeader(headerOfAMessage);
        inboxPage.switchToFrame();
        inboxPage.inputTextEmail(textOfAMessage);
        inboxPage.returnFromFrame();
        inboxPage.saveDraft();
        String currentURL = driver.getCurrentUrl();
        String idOfEMail = currentURL.replaceAll("\\D+", "").substring(0, 8);
        draftPage = inboxPage.openDrafts();
        Assert.assertTrue(draftPage.isElementPresent(draftPage.creatMessageLocatorWithIdForGettingAttribute(idOfEMail)));
        Assert.assertTrue(draftPage.findActualHeaderOfAMessage(idOfEMail).contains(headerOfAMessage));
        draftPage.singOut();
    }

    @Test
    public void testSendMail() throws InterruptedException {
        sentPage = inboxPage.openSent();
        String numberOfEmailBeforeSendEmail = sentPage.getCurrentNumberOfSentEmails();
        inboxPage.clickWriteEmailButton();
        inboxPage.inputReceiver(receiverEmail);
        inboxPage.inputHeader(headerOfAMessage);
        inboxPage.switchToFrame();
        inboxPage.inputTextEmail(textOfAMessage);
        inboxPage.returnFromFrame();
        inboxPage.saveDraft();
        String currentURL = driver.getCurrentUrl();
        String idOfEMail = currentURL.replaceAll("\\D+", "").substring(0, 8);
        draftPage = inboxPage.openDrafts();
        draftPage.openDraftMail(idOfEMail);
        draftPage.sendMail();
        draftPage.rediscoverDrafts();
        Assert.assertFalse(draftPage.isElementPresent(draftPage.creatMessageLocatorWithIdForGettingAttribute(idOfEMail)));
        draftPage.openSent();
        String numberOfEmailAfterSendEmail = sentPage.getCurrentNumberOfSentEmails();
        Assert.assertEquals(sentPage.addedEMailSent(numberOfEmailBeforeSendEmail, numberOfEmailAfterSendEmail), "1");
        sentPage.singOut();
    }

    @Test
    public void testLogOut() {
        inboxPage.singOut();
        Assert.assertTrue(loginPage.checkIsLoginFormDisplated());
    }
}
