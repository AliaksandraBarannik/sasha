package com.tat.barannik.hw12.PageObject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DraftsPage extends BasePage {
    protected final By sendMailButton = By.xpath("//*[@class='b-toolbar__btn__text' and text()='Send']");

    private String createdMessageLocatorForGetAttribute = "//div[contains(@data-id, '%s')][@class='b-datalist__item js-datalist-item']//a";

    public DraftsPage(WebDriver driver) {
        super(driver);
    }

    public void openDraftMail(String idOfEmail) throws InterruptedException {
        Thread.sleep(5000);
        waitForElementVisibility(creatMessageLocatorWithIdForGettingAttribute(idOfEmail));
        driver.findElement(creatMessageLocatorWithIdForGettingAttribute(idOfEmail)).click();
    }

    public void sendMail() {
        waitForElementToBeClickable(sendMailButton);
        driver.findElement(sendMailButton).click();
    }

    public By creatMessageLocatorWithIdForGettingAttribute(String idOfEMail) {
        By createdMessageLocatorWithIdForGettingAttribute = By.xpath(String.format(createdMessageLocatorForGetAttribute, idOfEMail));
        return createdMessageLocatorWithIdForGettingAttribute;
    }

    public String findActualHeaderOfAMessage(String idofEmail) {
        waitForElementVisibility(creatMessageLocatorWithIdForGettingAttribute(idofEmail));
        String actualHeaderOfAMessage = driver.findElement(creatMessageLocatorWithIdForGettingAttribute(idofEmail)).getAttribute("data-subject");
        return actualHeaderOfAMessage;
    }

}
