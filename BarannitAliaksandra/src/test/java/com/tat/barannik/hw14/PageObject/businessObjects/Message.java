package com.tat.barannik.hw14.PageObject.businessObjects;

public class Message {

    private String receiver;
    private String header;
    private String textOfMessage;

    public Message(String receiver, String header, String textOfMessage) {
        this.receiver = receiver;
        this.header = header;
        this.textOfMessage = textOfMessage;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getTextOfMessage() {
        return textOfMessage;
    }

    public void setTextOfMessage(String textOfMessage) {
        this.textOfMessage = textOfMessage;
    }
}
