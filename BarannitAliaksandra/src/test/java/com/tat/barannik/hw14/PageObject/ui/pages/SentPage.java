package com.tat.barannik.hw14.PageObject.ui.pages;

import com.tat.barannik.hw14.PageObject.utils.Helpers;

public class SentPage extends BasePage {

    public SentPage() {
        super();
    }

    public String getCurrentNumberOfSentEmails() {
        Helpers.waitForElementToBeClickable(sentButton);
        return driver.findElement(sentButton).getAttribute("data-title").replaceAll("\\D+", "");
    }

    public String addedEMailSent(String numberOfEmailBeforeSendEmail, String numberOfEmailAfterSendEmail) {
        int numberBeforeSend = Integer.parseInt(numberOfEmailBeforeSendEmail);
        int numberAfterSend = Integer.parseInt(numberOfEmailAfterSendEmail);
        int addedEmail = numberAfterSend - numberBeforeSend;
        String numberOfAddedEmail = Integer.toString(addedEmail);
        return numberOfAddedEmail;
    }
}
