package com.tat.barannik.hw14.PageObject.ui.pages;

import com.tat.barannik.hw14.PageObject.utils.Helpers;
import org.openqa.selenium.By;

public class NewMessagePage extends BasePage {

    private final By receiverField = By.cssSelector("textarea[data-original-name='To']");
    private final By headerField = By.cssSelector("input[class='b-input']");
    private final By saveDraftButton = By.xpath("//div[@data-name='saveDraft']");
    private final By inputTextOfEmailField = By.id("tinymce");

    public NewMessagePage() {
        super();
    }

    public void inputReceiver(String receiverEmail) {
        driver.findElement(receiverField).clear();
        driver.findElement(receiverField).sendKeys(receiverEmail);
    }

    public void inputHeader(String headerOfAMessage) {
        driver.findElement(headerField).clear();
        driver.findElement(headerField).sendKeys(headerOfAMessage);
    }

    public void inputTextEmail(String textOfAMessage) {
        driver.findElement(inputTextOfEmailField).clear();
        driver.findElement(inputTextOfEmailField).sendKeys(textOfAMessage);
    }

    public void saveDraft() {
        Helpers.waitForElementToBeClickable(saveDraftButton);
        driver.findElement(saveDraftButton).click();
    }
}
