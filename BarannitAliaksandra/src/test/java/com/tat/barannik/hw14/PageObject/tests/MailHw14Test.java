package com.tat.barannik.hw14.PageObject.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MailHw14Test extends BaseTest {

    @Test
    public void testLogin() {
        Assert.assertTrue(service.isSuccesfulLogin());
    }

    @Test
    public void testLogOut() {
        service.logOut();
        Assert.assertTrue(service.isSuccesfulLogOut());
    }

    @Test
    public void testSaveMailAsDraft() throws InterruptedException {
        service.createNewMessage(message);
        service.saveNewMessageAsDraft();
        String idOfMessage = service.getIdOfNewMessage();
        service.openDrafts();
        Assert.assertTrue(service.isMessageInDrafts(idOfMessage));
        Assert.assertTrue(service.isContainMessageTheHeader(idOfMessage, message.getHeader()));
        Assert.assertTrue(service.isContainMessageTheText(idOfMessage, message.getTextOfMessage()));
        Assert.assertTrue(service.isContainMessageTheReceiver(idOfMessage, message.getReceiver()));
    }

    @Test
    public void testSendMail() throws InterruptedException {
        service.openSent();
        String numberOfEmailBeforeSendEmail = service.getCurrentNumberOfSentEmails();
        service.createNewMessage(message);
        service.saveNewMessageAsDraft();
        String idOfMessage = service.getIdOfNewMessage();
        service.openDrafts();
        service.openDraftEmail(idOfMessage);
        service.sendEmail();
        service.openDrafts();
        Assert.assertFalse(service.isMessageNotInDrafts(idOfMessage));
        service.openSent();
        String numberOfEmailAfterSendEmail = service.getCurrentNumberOfSentEmails();
        Assert.assertEquals(service.getNumberOfAddedMessageInSent(numberOfEmailBeforeSendEmail, numberOfEmailAfterSendEmail), "1");
    }
}

