package com.tat.barannik.hw14.PageObject.ui.pages;

import com.tat.barannik.hw14.PageObject.utils.Helpers;
import org.openqa.selenium.By;

public class DraftsPage extends BasePage {

    private final By sendMailButton = By.xpath("//*[@class='b-toolbar__btn__text' and text()='Send']");
    private String createdMessageLocatorForGetAttribute = "//div[contains(@data-id, '%s')]/div[@class='b-datalist__item__body']/a";
    private String locatorForGettingAddress = createdMessageLocatorForGetAttribute + "//div[@class='b-datalist__item__addr']";
    private String locatorForGettingTextOfMessage=createdMessageLocatorForGetAttribute + "//div[@class='b-datalist__item__subj']//span";

    public DraftsPage() {
        super();
    }

    public String getCreatedMessageLocatorForGetAttribute() {
        return createdMessageLocatorForGetAttribute;
    }

    public String getLocatorForGettingAddress() {
        return locatorForGettingAddress;
    }

    public String getLocatorForGettingTextOfMessage() {
        return locatorForGettingTextOfMessage;
    }

    public void openDraftMail(String idOfEmail) {
        Helpers.waitForElementVisibility(createMessageLocatorWithIdForGettingAttribute(createdMessageLocatorForGetAttribute,idOfEmail));
        driver.findElement(createMessageLocatorWithIdForGettingAttribute(createdMessageLocatorForGetAttribute,idOfEmail)).click();
    }

    public void sendMail() {
        Helpers.waitForElementToBeClickable(sendMailButton);
        driver.findElement(sendMailButton).click();
    }

    public By createMessageLocatorWithIdForGettingAttribute(String locator, String idOfEMail) {
        By createdMessageLocatorWithIdForGettingAttribute = By.xpath(String.format(locator, idOfEMail));
        return createdMessageLocatorWithIdForGettingAttribute;
    }

    public String findActualReceiverOfAMessage(String idofEmail) {
        Helpers.waitForElementVisibility(createMessageLocatorWithIdForGettingAttribute(locatorForGettingAddress ,idofEmail));
        String actualReceiverOfAMessage = driver.findElement(createMessageLocatorWithIdForGettingAttribute(locatorForGettingAddress ,idofEmail)).getText();
        return actualReceiverOfAMessage;
    }

    public String findActualHeaderOfAMessage(String idofEmail) {
        Helpers.waitForElementVisibility(createMessageLocatorWithIdForGettingAttribute(createdMessageLocatorForGetAttribute,idofEmail));
        String actualHeaderOfAMessage = driver.findElement(createMessageLocatorWithIdForGettingAttribute(createdMessageLocatorForGetAttribute,idofEmail)).getAttribute("data-subject");
        return actualHeaderOfAMessage;
    }

    public String findActualTextOfAMessage(String idofEmail) {
        Helpers.waitForElementVisibility(createMessageLocatorWithIdForGettingAttribute(locatorForGettingTextOfMessage, idofEmail));
        String actualTextOfAMessage = driver.findElement(createMessageLocatorWithIdForGettingAttribute(locatorForGettingTextOfMessage,idofEmail)).getText();
        return actualTextOfAMessage;
    }
}
