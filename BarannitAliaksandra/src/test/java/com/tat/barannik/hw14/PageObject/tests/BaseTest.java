package com.tat.barannik.hw14.PageObject.tests;

import com.tat.barannik.hw14.PageObject.businessObjects.Message;
import com.tat.barannik.hw14.PageObject.businessObjects.User;
import com.tat.barannik.hw14.PageObject.services.Service;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    private static final String CHROME_PATH = "src/test/resources/chromedriver.exe";
    private static final String WEBSITE_ADDRESS = "https://mail.ru/";
    private final String eMail = "testnghw";
    private final String password = "1qaz2wsx";
    private String receiverEmail = "moyarabotasasha@mail.ru";
    private String headerOfAMessage = "test";
    private String textOfAMessage = "Hello world!";

    private static WebDriver driver;

    protected Service service;

    protected User user = new User(eMail, password);

    protected Message message = new Message(receiverEmail, headerOfAMessage, textOfAMessage);

    @BeforeMethod
    public void login() throws InterruptedException {
        service = new Service();
        service.login(user);
    }

    @AfterMethod
    public void logOut() throws InterruptedException {
        Thread.sleep(5000);
        if (!service.isOnMailRu()) {
            service.logOut();
        }
        if (service.isNotOnMailRu()) {
            service.closePopUpWindow();
        }
    }

    public static WebDriver getWebDriverInstance() {
        if (driver == null) {
            System.setProperty("webdriver.chrome.driver", CHROME_PATH);
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }
        return driver;
    }

    @BeforeClass
    public void setUp() {
        driver = getWebDriverInstance();
        driver.get(WEBSITE_ADDRESS);
    }

    @AfterClass
    public void tearDoun() {
        driver.quit();
    }
}