package com.tat.barannik.hw17;

import org.junit.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class RestAPITest {

    String URLForListOfUsers = "http://jsonplaceholder.typicode.com/posts";
    String URLForUser = URLForListOfUsers + "/1";

    @Test
    public void checkStatusCode() {
        given().contentType(JSON)
                .get(URLForListOfUsers)
                .then()
                .contentType(JSON)
                .statusCode(200);
    }

    @Test
    public void checkHeaders() {
        given().contentType(JSON)
                .get(URLForListOfUsers)
                .then()
                .contentType(JSON)
                .header("Content-Type", equalTo("application/json; charset=utf-8"));
    }

    @Test
    public void checkBody() {
        given().contentType(JSON)
                .when()
                .get(URLForListOfUsers)
                .then()
                .body("size()", equalTo(100));
    }

    @Test
    public void createNewUser() {
        String idOfUser = given().contentType(JSON)
                .body(new User("foo", "bar", 1))
                .post(URLForListOfUsers)
                .then().contentType(JSON).statusCode(201)
                .body("isEmpty()", is(false))
                .extract().path("id").toString();
        System.out.println(idOfUser);
    }

    @Test
    public void updateNewUser() {
        given().contentType(JSON)
                .body(new User(1, "foo2", "bar", 1))
                .put(URLForUser)
                .then()
                .contentType(JSON)
                .statusCode(200)
                .body("id", equalTo(1));
    }

    @Test
    public void deleteTheFirstUser() {
        given().contentType(JSON)
                .delete(URLForUser)
                .then().statusCode(200);
    }
}