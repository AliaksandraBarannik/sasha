package com.tat.barannik.hw17;

public class User {
    private int id;
    private String title;
    private String body;
    private int userId;

    public User(String title, String body, int userId) {
        this.title = title;
        this.body = body;
        this.userId = userId;
    }

    public User(int id, String title, String body, int userId) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.userId = userId;
    }
}
