package com.tat.barannik.hw13;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class CheckBoxRadioPage extends MainPage {

    @FindBy(css = "label[for='radio-1']")
    private WebElement selestionNewYork;

    @FindBy(css = "label[for='checkbox-1']")
    private WebElement selectionTwoStar;

    @FindBy(xpath = "//div[@class='widget']//label[(text()='New York') and (contains(@class, 'ui-checkboxradio-checked ui-state-active'))]")
    private WebElement selectedNewYork;

    @FindBy(xpath = "//div[@class='widget']//label[(text()='2 Star') and (contains(@class, 'ui-checkboxradio-checked ui-state-active'))]")
    private WebElement selectedTwooStar;

    public void selestNewYork() {
        new Actions(driver).click(selestionNewYork).build().perform();
    }

    public void selestTwoStar() {
        new Actions(driver).click(selectionTwoStar).build().perform();
    }

    public Boolean isSelectedNewYork() {
        selectedNewYork.click();
        return true;
    }

    public Boolean isSelectedTwooStar() {
        selectedTwooStar.click();
        return true;
    }

    public CheckBoxRadioPage(WebDriver driver) {
        super(driver);
    }

}
