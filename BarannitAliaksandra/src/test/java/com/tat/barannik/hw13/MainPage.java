package com.tat.barannik.hw13;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class MainPage extends BasePage {
    @FindBy(xpath = "//div[@id='sidebar']//a[contains(@href, 'droppable')]")
    protected WebElement droppableButon;

    @FindBy(xpath = "//div[@id='sidebar']//a[contains(@href, 'spinner')]")
    protected WebElement spinnerButon;

    @FindBy(xpath = "//*[@id='sidebar']//a[contains(@href,'checkboxradio')]")
    protected WebElement checkBoxRadioButton;

    @FindBy(xpath = "//*[@id='sidebar']//a[contains(@href,'selectmenu')]")
    protected WebElement selectMenuButton;

    @FindBy(xpath = "//*[@id='sidebar']//a[contains(@href,'tooltip')]")
    protected WebElement tooltipButton;

    @FindBy(css = "iframe[class='demo-frame']")
    protected WebElement frame;

    @FindBy(xpath = "//div[@class='constrain']//a[@title='jQuery UI']")
    protected WebElement jQueryUIButton;

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public DroppablePage openDroppablePage() {
        waitForElementToBeClickable(droppableButon);
        new Actions(driver).click(droppableButon).build().perform();
        return new DroppablePage(driver);
    }

    public CheckBoxRadioPage openCheckBoxRadioPage() {
        waitForElementToBeClickable(checkBoxRadioButton);
        new Actions(driver).click(checkBoxRadioButton).build().perform();
        return new CheckBoxRadioPage(driver);
    }

    public TooltipPage openTooltipPage() {
        new Actions(driver).click(tooltipButton).build().perform();
        return new TooltipPage(driver);
    }

    public SpinnerPage openSpinnerPage() {
        new Actions(driver).click(spinnerButon).build().perform();
        return new SpinnerPage(driver);
    }

    public SelectMenuPage openSelectMenuPage() {
        new Actions(driver).click(selectMenuButton).build().perform();
        return new SelectMenuPage(driver);
    }

    public MainPage openMainPage() {
        new Actions(driver).click(jQueryUIButton).build().perform();
        return new MainPage(driver);
    }

    public void switchToFrame() {
        waitForElementVisibility(frame);
        driver.switchTo().frame(frame);
    }

    public void returnFromFrame() {
        driver.switchTo().defaultContent();
    }
}
