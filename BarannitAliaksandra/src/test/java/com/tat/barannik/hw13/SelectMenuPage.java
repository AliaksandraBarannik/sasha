package com.tat.barannik.hw13;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class SelectMenuPage extends MainPage {

    @FindBy(id = "speed-button")
    private WebElement speedButton;

    @FindBy(id = "ui-id-4")
    private WebElement fastSpeedButton;

    @FindBy(id = "number-button")
    private WebElement numberButton;

    @FindBy(id = "ui-id-8")
    private WebElement numberThreeButton;

    public SelectMenuPage(WebDriver driver) {
        super(driver);
    }

    public void selectSpeed() {
        new Actions(driver).click(speedButton).build().perform();
        new Actions(driver).click(fastSpeedButton).build().perform();
    }

    public void selectNumber() {
        new Actions(driver).click(numberButton).build().perform();
        new Actions(driver).click(numberThreeButton).build().perform();
    }

    public String getSpeed() {
        return speedButton.getText();
    }

    public String getNumber() {
        return numberButton.getText();
    }
   /* public String selectSpeed() {
        waitForElementVisibility(frame);
        driver.switchTo().frame(frame);
        new Actions(driver).click(speedButton).build().perform();
        new Actions(driver).click(fastSpeedButton).build().perform();
        String setSpeed = speedButton.getText();
        driver.switchTo().defaultContent();
        return setSpeed;
    }

    public String selectNumber() {
        waitForElementVisibility(frame);
        driver.switchTo().frame(frame);
        new Actions(driver).click(numberButton).build().perform();
        new Actions(driver).click(numberThreeButton).build().perform();
        String setNumber = numberButton.getText();
        driver.switchTo().defaultContent();
        return setNumber;
    }*/
}
