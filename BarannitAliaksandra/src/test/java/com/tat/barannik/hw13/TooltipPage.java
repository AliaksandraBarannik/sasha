package com.tat.barannik.hw13;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class TooltipPage extends MainPage {

    @FindBy(xpath = "//a[contains(@href, '#')]")
    private WebElement tooltips;

    @FindBy(css = "div.ui-helper-hidden-accessible div:nth-child(1)")
    private WebElement toolTipText;


    public TooltipPage(WebDriver driver) {
        super(driver);
    }

    public String hoverMouseAndGetTooltipText() {
        new Actions(driver).moveToElement(tooltips).build().perform();
        String textFromTooltip = toolTipText.getText();
        return textFromTooltip;
    }
}
