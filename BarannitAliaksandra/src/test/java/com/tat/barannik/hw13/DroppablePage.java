package com.tat.barannik.hw13;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class DroppablePage extends MainPage {

    @FindBy(id = "draggable")
    private WebElement smallSquare;

    @FindBy(id = "droppable")
    private WebElement bigSquare;

    @FindBy(xpath = "//*[@id='droppable']//p[text()='Dropped!']")
    protected WebElement droppedBigSquare;

    public DroppablePage(WebDriver driver) {
        super(driver);
    }

    public void dragAndDropSmallSquare() {
        waitForElementVisibility(smallSquare);
        waitForElementVisibility(bigSquare);
        new Actions(driver).dragAndDrop(smallSquare, bigSquare).build().perform();
    }
}
