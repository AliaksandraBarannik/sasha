package com.tat.barannik.hw13;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class SpinnerPage extends MainPage {

    @FindBy(xpath = "//button[@id='setvalue']")
    private WebElement setValueToFiveButton;

    @FindBy(css = "input[id='spinner']")
    private WebElement inputField;

    public SpinnerPage(WebDriver driver) {
        super(driver);
    }

    public void setFive() {
        waitForElementToBeClickable(setValueToFiveButton);
        new Actions(driver).click(setValueToFiveButton).build().perform();
    }


    public String getValueFromInputField() {
        String value = inputField.getAttribute("aria-valuenow");
        return value;
    }
}
