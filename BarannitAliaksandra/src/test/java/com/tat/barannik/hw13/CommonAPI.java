package com.tat.barannik.hw13;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class CommonAPI {

    public abstract WebDriver getDriver();

    public void waitForElementVisibility(WebElement webElement) {
        new WebDriverWait(getDriver(), 10).until(ExpectedConditions.visibilityOf(webElement));
    }

    public void waitForElementToBeClickable(WebElement webElement) {
        new WebDriverWait(getDriver(), 10).until(ExpectedConditions.elementToBeClickable(webElement));
    }
}
