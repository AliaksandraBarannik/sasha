package com.tat.barannik.hw13;


import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ActionsTest extends BaseTest {
    MainPage mainPage;

    @BeforeMethod
    public void initMainPage() {
        mainPage = new MainPage(driver);
    }

    @Test
    public void returnMainPage() {
        mainPage.openMainPage();
    }

    @Test
    public void testDragAndDropSmallSquare() {
        DroppablePage droppablePage = mainPage.openDroppablePage();
        droppablePage.switchToFrame();
        droppablePage.dragAndDropSmallSquare();
        Assert.assertTrue(droppablePage.droppedBigSquare.getText().contains("Dropped!"));
        droppablePage.returnFromFrame();
    }

    @Test
    public void testCheckBoxRadio() {
        CheckBoxRadioPage checkBoxRadioPage = mainPage.openCheckBoxRadioPage();
        checkBoxRadioPage.switchToFrame();
        checkBoxRadioPage.selestNewYork();
        Assert.assertTrue(checkBoxRadioPage.isSelectedNewYork());
        checkBoxRadioPage.selestTwoStar();
        Assert.assertTrue(checkBoxRadioPage.isSelectedTwooStar());
        checkBoxRadioPage.returnFromFrame();
        mainPage.openMainPage();
    }

    @Test
    public void testSelectMenu() {
        SelectMenuPage selectMenuPage = mainPage.openSelectMenuPage();
        selectMenuPage.switchToFrame();
        selectMenuPage.selectSpeed();
        selectMenuPage.selectNumber();
        String setSpeed = selectMenuPage.getSpeed();
        Assert.assertEquals(setSpeed, "Fast");
        String setNumber = selectMenuPage.getNumber();
        Assert.assertEquals(setNumber, "3");
        selectMenuPage.returnFromFrame();
    }

    @Test
    public void testTooltip() {
        TooltipPage tooltipPage = mainPage.openTooltipPage();
        tooltipPage.switchToFrame();
        String tooltipText = tooltipPage.hoverMouseAndGetTooltipText();
        Assert.assertEquals(tooltipText, "That's what this widget is");
        tooltipPage.returnFromFrame();
    }

    @Test
    public void testSetFive() {
        SpinnerPage spinnerPage = mainPage.openSpinnerPage();
        spinnerPage.switchToFrame();
        spinnerPage.setFive();
        Assert.assertEquals(spinnerPage.getValueFromInputField(), "5");
        spinnerPage.returnFromFrame();
    }
}
