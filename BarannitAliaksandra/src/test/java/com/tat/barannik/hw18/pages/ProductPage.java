package com.tat.barannik.hw18.pages;

import com.tat.barannik.hw18.utils.Helpers;
import org.openqa.selenium.By;

public class ProductPage extends BasePage {
    private By addToBasketButton = By.cssSelector("#isCartBtn_btn");

    public ProductPage() {
        super();
    }

    public BasketPage addProductToBasketAndOpenBasket() {
        Helpers.waitForElementVisibility(addToBasketButton);
        driver.findElement(addToBasketButton).click();
        return new BasketPage();
    }
}

