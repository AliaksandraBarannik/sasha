package com.tat.barannik.hw18.pages;

import com.tat.barannik.hw18.utils.Helpers;
import org.openqa.selenium.By;

public class BasketPage extends BasePage {

    private By listOfProductsInBasket = By.cssSelector("#ShopCart > div");

    public BasketPage() {
        super();
    }

    public String getTextOfProductInBasket() {
        Helpers.waitForElementVisibility(listOfProductsInBasket);
        String textOfProduct = driver.findElements(listOfProductsInBasket).get(0).getText();
        return textOfProduct;
    }
}
