package com.tat.barannik.hw18.steps;

import com.tat.barannik.hw18.services.Service;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class EbayCheckProductInBasketDef {

    Service service;

    @Given("^I opened ebay HomePage to search the product$")
    public void iOpenedEbayHomePage() {
        service = new Service();
        service.open();
        service.makeScreenshot();
    }

    @When("^I searched product \"([^\"]*)\"$")
    public void iSearchedTheProduct(String product) {
        service.searchProduct(product);
        service.makeScreenshot();
    }

    @And("^I clicked on the first product$")
    public void iClickedOnTheFirstProduct() {
        service.clickOnFirstProduct();
        service.makeScreenshot();
    }

    @And("^I added the product to the basket and opened the basket$")
    public void iAddedTheProductToTheBasketAndOpenedTheBasket() {
        service.addProductToBasketAndOpenBasket();
        service.makeScreenshot();
    }

    @Then("^I checked the product \"([^\"]*)\" was in the basket$")
    public void iCheckedTheProductWasInTheBasket(String expectPhrase) {
        Assert.assertTrue(service.getTextOfProductInBasket().toUpperCase().contains(expectPhrase.toUpperCase()));
        service.makeScreenshot();
    }

    @Then("^the product \"([^\"]*)\" should be the first in the Search Result grid$")
    public void theProductShouldBeTheFirstInTheSearchResultGrid(String expectPhrase) {
        String textOfProduct = service.getTextFromFirstProduct();
        Assert.assertTrue(textOfProduct.toUpperCase().contains(expectPhrase.toUpperCase()));
        service.makeScreenshot();
    }
}
