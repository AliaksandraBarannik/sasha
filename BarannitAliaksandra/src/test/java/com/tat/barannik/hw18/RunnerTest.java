package com.tat.barannik.hw18;

import com.tat.barannik.hw18.driver.Driver;
import cucumber.api.CucumberOptions;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(CustomRunner.class)
@CucumberOptions(
        features = "src/test/resources/featuresForHw18",
        strict = true,
        plugin = {
                "pretty", "json:target/Cucumber.json",
                "html:target/cucumber-html-report",
        },
        glue = "com.tat.barannik.hw18.steps",
        tags = "@ebay"
)

public class RunnerTest {

    @AfterClass
    public static void closeDriver() {
        Driver.tearDown();
    }
}
