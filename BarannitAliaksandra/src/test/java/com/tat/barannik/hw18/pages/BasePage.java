package com.tat.barannik.hw18.pages;

import com.tat.barannik.hw18.driver.Driver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BasePage {
    private final String WEBSITE_ADDRESS = "https://ebay.com/";

    protected WebDriver driver;

    public BasePage() {
        this.driver = Driver.getInstance();
    }

    public HomePage open() {
        driver.get(WEBSITE_ADDRESS);
        return new HomePage();
    }

    public WebElement highlightElement(WebElement elem) {
        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].style.backgroundColor = 'red'", elem);
        }
        return elem;
    }
}
