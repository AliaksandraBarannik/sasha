package com.tat.barannik.hw18.utils;


import com.tat.barannik.hw18.driver.Driver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tools.ant.util.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class Helpers {

    private static final Logger log = LogManager.getLogger("Helpers");

    private static String setNewFileName() {
        String newFileName = "scr_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()) + ".png";
        return newFileName;
    }

    public static void waitForElementVisibility(By locator) {
        new WebDriverWait(Driver.getInstance(), 30).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static Boolean isElementPresent(By element) {
        return Driver.getInstance().findElements(element).size() > 0;
    }

    public static void makeScreenshot() {
        File screenshot = ((TakesScreenshot) Driver.getInstance()).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.getFileUtils().copyFile(screenshot, new File("src/test/resources/screenshots/" + Helpers.setNewFileName()));
            log.info("File was created successfully");
        } catch (IOException e) {
            log.error(e);
        }
    }
}
