package com.tat.barannik.hw18.services;

import com.tat.barannik.hw18.pages.*;
import com.tat.barannik.hw18.utils.Helpers;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Service {

    BasePage basePage;
    BasketPage basketPage;
    HomePage homePage;
    ProductPage productPage;
    ResultPage resultPage;

    private final static Logger logger = LogManager.getLogger("Service");

    public void open() {
        basePage = new BasePage();
        homePage = basePage.open();
        logger.info("EbayPage is opened");
    }

    public void searchProduct(String product) {
        logger.warn("You should close pop-up window ");
        homePage.closePopUpWindowOnHomePage();
        homePage.inputSearchField(product);
        resultPage = homePage.clickSearchButton();
    }

    public String getTextFromFirstProduct() {
        return resultPage.getTextFromFirstProduct();
    }

    public void clickOnFirstProduct() {
        productPage = resultPage.clickOnFirstProduct();
        logger.info("You will go over ProductPage");
    }

    public void addProductToBasketAndOpenBasket() {
        logger.warn("Take sure that the correct product is displayed");
        basketPage = productPage.addProductToBasketAndOpenBasket();
    }

    public String getTextOfProductInBasket() {
        logger.info("Checking the text of the first element with the given text");
        return basketPage.getTextOfProductInBasket();
    }

    public void makeScreenshot() {
        Helpers.makeScreenshot();
    }
}
