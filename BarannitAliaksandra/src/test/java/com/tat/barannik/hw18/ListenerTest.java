package com.tat.barannik.hw18;

import com.tat.barannik.hw18.utils.Helpers;
import org.junit.runner.Description;
import org.junit.runner.notification.RunListener;

public class ListenerTest extends RunListener {

    @Override
    public void testFinished(Description description) {
        Helpers.makeScreenshot();
        System.out.println("Listener.testFinished: made a screenshot " + description.getDisplayName());
    }
}
