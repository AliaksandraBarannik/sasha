package com.tat.barannik.hw15.PageObject.ui.pages;

import com.tat.barannik.hw15.PageObject.tests.Driver;
import com.tat.barannik.hw15.PageObject.utils.Helpers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class BasePage {

    private By popUpWindow = By.cssSelector("#extension-bubble-close");
    private final By logOutButton = By.cssSelector("#PH_logoutLink");
    private final By iframe = By.cssSelector("iframe[title='{#aria.rich_text_area}']");
    private final By openDraftButton = By.xpath("//*[@class='b-nav__item__text' and text()='Drafts']");
    private final By writeEmailButton = By.cssSelector("div[id='LEGO'] a[data-name='compose']");
    protected final By sentButton = By.linkText("Sent");
    protected WebDriver driver;
    public final By confirmationSingIn = By.id("PH_user-email");

    public BasePage() {
        this.driver = Driver.getInstance();
    }

    public LoginPage singOut() {
        Helpers.waitForElementVisibility(logOutButton);
        driver.findElement(logOutButton).click();
        return new LoginPage();
    }

    public By getPopUpWindow() {
        return popUpWindow;
    }

    public void closePopUpWindow() {
        driver.findElement(popUpWindow).click();
    }

    public DraftsPage openDrafts() throws InterruptedException {
        Thread.sleep(5000);
        driver.findElement(openDraftButton).click();
        return new DraftsPage();
    }

    public NewMessagePage createMessage() {
        driver.findElement(writeEmailButton).click();
        return new NewMessagePage();
    }

    public SentPage openSent() {
        driver.findElement(sentButton).click();
        return new SentPage();
    }

    public void switchToFrame() {
        driver.switchTo().frame(driver.findElement(iframe));
    }

    public void returnFromFrame() {
        driver.switchTo().defaultContent();
    }
}
