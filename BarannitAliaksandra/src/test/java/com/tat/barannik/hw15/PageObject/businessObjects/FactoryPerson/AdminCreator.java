package com.tat.barannik.hw15.PageObject.businessObjects.FactoryPerson;

public class AdminCreator implements PersonCreator {

    @Override
    public Person create(String login, String password) {
        return new Admin(login, password);
    }
}
