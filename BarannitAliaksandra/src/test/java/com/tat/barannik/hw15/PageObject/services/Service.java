package com.tat.barannik.hw15.PageObject.services;

import com.tat.barannik.hw15.PageObject.businessObjects.FactoryPerson.Person;
import com.tat.barannik.hw15.PageObject.businessObjects.Message;
import com.tat.barannik.hw15.PageObject.tests.Driver;
import com.tat.barannik.hw15.PageObject.ui.pages.*;
import com.tat.barannik.hw15.PageObject.utils.Helpers;

public class Service {

    NewMessagePage newMessagePage;
    InboxPage inboxPage;
    DraftsPage draftPage;
    SentPage sentPage;
    LoginPage loginPage;
    BasePage basePage;

    public void login(Person person) throws InterruptedException {
        basePage = new BasePage();
        loginPage = new LoginPage();
        loginPage.inputLoginField(person.getLogin());
        loginPage.inputPasswordField(person.getPassword());
        inboxPage = loginPage.clickSingInButton();
    }

    public void createNewMessage(Message message) {
        newMessagePage = inboxPage.createMessage();
        newMessagePage.inputReceiver(message.getReceiver());
        newMessagePage.inputHeader(message.getHeader());
        newMessagePage.switchToFrame();
        newMessagePage.inputTextEmail(message.getTextOfMessage());
        newMessagePage.returnFromFrame();
    }

    public void saveNewMessageAsDraft() {
        newMessagePage.saveDraft();
    }

    public String getIdOfNewMessage() {
        String currentURL = Driver.getInstance().getCurrentUrl();
        String idOfEMail = currentURL.replaceAll("\\D+", "").substring(0, 8);
        return idOfEMail;
    }

    public Boolean isMessageInDrafts(String idOfEMail) {
        return Helpers.isElementPresent(draftPage.createMessageLocatorWithIdForGettingAttribute(draftPage.getCreatedMessageLocatorForGetAttribute(),idOfEMail));
    }

    public Boolean isMessageNotInDrafts(String idOfEMail) {
        Helpers.waitForElementAbsent(draftPage.createMessageLocatorWithIdForGettingAttribute(draftPage.getCreatedMessageLocatorForGetAttribute(),idOfEMail));
        return Helpers.isElementPresent(draftPage.createMessageLocatorWithIdForGettingAttribute(draftPage.getCreatedMessageLocatorForGetAttribute(),idOfEMail));
    }

    public String getNumberOfAddedMessageInSent(String numberOfEmailBeforeSendEmail, String numberOfEmailAfterSendEmail) {
        return sentPage.addedEMailSent(numberOfEmailBeforeSendEmail, numberOfEmailAfterSendEmail);
    }

    public Boolean isContainMessageTheHeader(String idOfEmail, String header) {
        return draftPage.findActualHeaderOfAMessage(idOfEmail).contains(header);
    }

    public Boolean isContainMessageTheReceiver(String idOfEmail, String receiver) {
        return draftPage.findActualReceiverOfAMessage(idOfEmail).contains(receiver);
    }

    public Boolean isContainMessageTheText(String idOfEmail, String text) {
        return draftPage.findActualTextOfAMessage(idOfEmail).contains(text);
    }

    public Boolean isSuccesfulLogin() {
        return Helpers.isElementDisplayed(inboxPage.confirmationSingIn);
    }

    public Boolean isOnMailRu() {
        return Helpers.isElementPresent(loginPage.getLoginField());
    }

    public Boolean isNotOnMailRu() {
        return Helpers.isElementPresent(basePage.getPopUpWindow());
    }

    public void closePopUpWindow() {
        basePage.closePopUpWindow();
    }

    public Boolean isSuccesfulLogOut() {
        return Helpers.isElementDisplayed(loginPage.getLoginField());
    }

    public void openDrafts() throws InterruptedException {
        draftPage = inboxPage.openDrafts();
    }

    public void openSent() {
        sentPage = inboxPage.openSent();
    }

    public String getCurrentNumberOfSentEmails() {
        return sentPage.getCurrentNumberOfSentEmails();
    }

    public void openDraftEmail(String idOfEMail) {
        draftPage.openDraftMail(idOfEMail);
    }

    public void sendEmail() {
        draftPage.sendMail();
    }

    public void logOut() {
        inboxPage.singOut();
    }
}
