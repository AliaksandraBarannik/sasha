package com.tat.barannik.hw15.PageObject.businessObjects.FactoryPerson;

public interface PersonCreator {

    Person create(String login, String password);

}
