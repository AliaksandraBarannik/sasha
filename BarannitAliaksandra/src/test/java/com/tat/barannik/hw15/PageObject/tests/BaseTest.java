package com.tat.barannik.hw15.PageObject.tests;


import com.tat.barannik.hw15.PageObject.businessObjects.FactoryPerson.Person;
import com.tat.barannik.hw15.PageObject.businessObjects.FactoryPerson.PersonCreator;
import com.tat.barannik.hw15.PageObject.businessObjects.FactoryPerson.UserCreator;
import com.tat.barannik.hw15.PageObject.businessObjects.Message;
import com.tat.barannik.hw15.PageObject.services.Service;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class BaseTest {

    private final String eMail = "testnghw";
    private final String password = "1qaz2wsx";
    private String receiverEmail = "moyarabotasasha@mail.ru";
    private String headerOfAMessage = "test";
    private String textOfAMessage = "Hello world!";
    private final String WEBSITE_ADDRESS = "https://mail.ru/";

    protected WebDriver driver;

    Service service;

    PersonCreator creator = new UserCreator();
    Person person = creator.create(eMail, password);

    protected Message message = new Message.MessageBuilder().receiver(receiverEmail).header(headerOfAMessage).textOfMessage(textOfAMessage).build();

    @BeforeMethod
    public void login() throws InterruptedException {
        service = new Service();
        service.login(person);
    }

    @AfterMethod
    public void logOut() throws InterruptedException {
        Thread.sleep(5000);
        if (!service.isOnMailRu()) {
            service.logOut();
        }
        if (service.isNotOnMailRu()) {
            service.closePopUpWindow();
        }
    }

    public WebDriver getDriver() {
        return driver;
    }

    @BeforeClass
    public void setUp() {
        driver = Driver.getInstance();
        driver.get(WEBSITE_ADDRESS);
    }

    @AfterClass
    public void tearDoun() {
        driver.quit();
    }
}