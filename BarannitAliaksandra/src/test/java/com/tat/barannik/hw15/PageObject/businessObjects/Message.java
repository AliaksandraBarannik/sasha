package com.tat.barannik.hw15.PageObject.businessObjects;

public class Message {

    private String receiver;
    private String header;
    private String textOfMessage;

    private Message(MessageBuilder builder) {
        this.receiver = builder.receiver;
        this.header = builder.header;
        this.textOfMessage = builder.textOfMessage;
    }

    public static class MessageBuilder {
        private String receiver;
        private String header;
        private String textOfMessage;

        public MessageBuilder receiver(String receiver) {
            this.receiver = receiver;
            return this;
        }

        public MessageBuilder header(String header) {
            this.header = header;
            return this;
        }

        public MessageBuilder textOfMessage(String textOfMessage) {
            this.textOfMessage = textOfMessage;
            return this;
        }

        public Message build() {
            return new Message(this);
        }
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getTextOfMessage() {
        return textOfMessage;
    }

    public void setTextOfMessage(String textOfMessage) {
        this.textOfMessage = textOfMessage;
    }
}
