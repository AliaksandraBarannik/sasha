package com.tat.barannik.hw15.PageObject.businessObjects.FactoryPerson;

public class UserCreator implements PersonCreator {

    @Override
    public Person create(String login, String password) {
        return new User(login, password);
    }
}
