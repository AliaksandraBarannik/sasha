package com.tat.barannik.hw15.PageObject.ui.pages;

import com.tat.barannik.hw15.PageObject.utils.Helpers;
import org.openqa.selenium.By;

public class LoginPage extends BasePage {

    private final By loginField = By.cssSelector("input[id='mailbox:login']");
    private final By passwordField = By.xpath("//input[@id='mailbox:password']");
    private final By singInButton = By.cssSelector("input.o-control");

    public LoginPage() {
        super();
    }

    public By getLoginField() {
        return loginField;
    }

    public void inputLoginField(String eMail) {
        Helpers.waitForElementVisibility(loginField);
        driver.findElement(loginField).clear();
        driver.findElement(loginField).sendKeys(eMail);
    }

    public void inputPasswordField(String password) {
        Helpers.waitForElementVisibility(passwordField);
        driver.findElement(passwordField).clear();
        driver.findElement(passwordField).sendKeys(password);
    }

    public InboxPage clickSingInButton() throws InterruptedException {
        driver.findElement(singInButton).click();
        return new InboxPage();
    }
}
