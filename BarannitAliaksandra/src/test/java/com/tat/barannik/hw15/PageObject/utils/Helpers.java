package com.tat.barannik.hw15.PageObject.utils;

import com.tat.barannik.hw15.PageObject.tests.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Helpers {

    public static void waitForElementVisibility(By locator) {
        new WebDriverWait(Driver.getInstance(), 30).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static void waitForElementAbsent(By locator) {
        for (int i = 1; i <= 30; i++) {
            try {
                new WebDriverWait(Driver.getInstance(), 1).until(ExpectedConditions.visibilityOfElementLocated(locator));
            } catch (TimeoutException e) {
                break;
            }
        }
    }

    public static void waitForElementToBeClickable(By locator) {
        new WebDriverWait(Driver.getInstance(), 30).until(ExpectedConditions.elementToBeClickable(locator));
    }

    public static boolean isElementDisplayed(By element) {
        waitForElementVisibility(element);
        return Driver.getInstance().findElement(element).isDisplayed();
    }

    public static Boolean isElementPresent(By element) {
        return Driver.getInstance().findElements(element).size() > 0;
    }
}
