package com.tat.barannik.hw15.PageObject.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Driver {

    public final static String CHROME_PATH = "src/test/resources/chromedriver.exe";

    private static DriverDecorator driver;

    private Driver() {
    }

    public static WebDriver getInstance() {
        if (driver == null) {
            System.setProperty("webdriver.chrome.driver", CHROME_PATH);
            WebDriver driver1 = new ChromeDriver();
            driver = new DriverDecorator(driver1);
            driver.manage();
        }
        return driver;
    }
}
