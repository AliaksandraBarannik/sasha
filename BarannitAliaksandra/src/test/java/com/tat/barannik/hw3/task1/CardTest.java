package com.tat.barannik.hw3.task1;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CardTest {

    @Test
    public void getBalance0() {
        Card card = new Card("Sasha");
        double balance = card.getBalance();
        Assert.assertEquals(0.0, balance, 0);
    }

    @Test
    public void getName() {
        Card card = new Card("Sasha", 4.5);
        String name = card.getName();
        Assert.assertEquals("Sasha", name);
    }

    @Test
    public void getBalance() {
        Card card = new Card("Sasha", 4.5);
        double balance = card.getBalance();
        Assert.assertEquals(4.5, balance, 0);
    }

    @Test
    public void setName() {
        Card card = new Card();
        card.setName("Sasha");
        String name = card.getName();
        Assert.assertEquals("Sasha", name);
    }

    @Test
    public void setBalance() {
        Card card = new Card();
        card.setBalance(4.5);
        double balance = card.getBalance();
        Assert.assertEquals(4.5, balance, 0);
    }

    @Test(expected = RuntimeException.class)
    public void setBalanceException() throws RuntimeException {
        Card card = new Card();
        card.setBalance(-4.0);
    }

    @Test
    public void depositMoney() {
        Card card = new Card("Sasha", 4.5);
        double balance = card.depositMoney(11.2);
        Assert.assertEquals(15.7, balance, 0);
    }

    @Test(expected = RuntimeException.class)
    public void depositMoneyException() throws RuntimeException {
        Card card = new Card("Sasha", 4.0);
        card.depositMoney(-4.0);
    }

    @Test
    public void withdrawMoney() {
        Card card = new Card("Sasha", 4.5);
        card.withdrawMoney(1.3);
        double balance = card.getBalance();
        Assert.assertEquals(3.2, balance, 0);
    }

    @Test(expected = RuntimeException.class)
    public void withdrawMoneyException() throws RuntimeException {
        Card card = new Card("Sasha", 4.5);
        card.withdrawMoney(9.0);
    }

    @Test(expected = RuntimeException.class)
    public void withdrawMoneyExceptionNegative() throws RuntimeException {
        Card card = new Card("Sasha", 4.5);
        card.withdrawMoney(-1.0);
    }

    @Test
    public void getBalanceAfterConversation() {
        Card card = new Card("Sasha", 4.5);
        double balance = card.getBalanceAfterConversation(0.2);
        Assert.assertEquals(0.9, balance, 0.01);
    }

    @Test(expected = RuntimeException.class)
    public void getBalanceAfterConversationNegativeCurrency() throws RuntimeException {
        Card card = new Card("Sasha", 4.5);
        card.getBalanceAfterConversation(-0.2);
    }
}