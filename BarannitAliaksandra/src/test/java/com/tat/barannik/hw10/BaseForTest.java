package com.tat.barannik.hw10;

//import com.epam.tat.module4.Calculator;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

public class BaseForTest {

    //Calculator calculator;

    //@BeforeMethod
   // public void setUp() {
    //    calculator = new Calculator();
    //}
    //@AfterMethod
    //public void tearDown() {
       // calculator = null;
    //}

    @DataProvider(name = "Numbers of long type for sum")
    public static Object[][] numbersLongTypeForSum() {
        return new Object[][]{
                {-1L, 0L, -1L},
                {22L, 10L, 32L},
                {1L, 0L, 1L},
                {-1L, -1L, -2L},
                {0L, 0L, 0L},
                {2L, -3L, -1L}
        };
    }

    @DataProvider(name = "Numbers of double type for sum")
    public static Object[][] numbersDoubleTypeForSum() {
        return new Object[][]{
                {-1.0, 0.0, -1.0},
                {22.3, 10.1, 32.4},
                {0.0, 1.0, 1.0},
                {-1.0, -1.0, -2.0},
                {0.0, 0.0, 0.0},
                {2.0, -1.0, 1.0}
        };
    }

    @DataProvider(name = "Numbers of long type for sub")
    public static Object[][] numbersLongTypeForSub() {
        return new Object[][]{
                {-10L, 0L, -10L},
                {22L, 10L, 12L},
                {1L, 0L, 1L},
                {-1L, -1L, 0L},
                {0L, 0L, 0L},
                {3L, -2L, 5L}
        };
    }

    @DataProvider(name = "Numbers of double type for sub")
    public static Object[][] numbersDoubleTypeForSub() {
        return new Object[][]{
                {-10.0, 0.0, -10.0},
                {22.3, 10.1, 12.2},
                {0.0, 1.0, -1.0},
                {-1.0, -1.0, 0.0},
                {0.0, 0.0, 0.0},
                {2.0, -3.0, 5.0}
        };
    }

    @DataProvider(name = "Numbers of long type for mult")
    public static Object[][] numbersLongTypeForMult() {
        return new Object[][]{
                {-10L, 1L, -10L},
                {10L, 0L, 0L},
                {3L, 2L, 6L},
                {-1L, -1L, 1L},
                {0L, 0L, 0L},
                {-10L, 0L, 0}
        };
    }

    @DataProvider(name = "Numbers of double type for mult")
    public static Object[][] numbersDoubleTypeForMult() {
        return new Object[][]{
                {-10.0, 1.0, -10.0},
                {10.0, 0.0, 0.0},
                {10.0, 1.0, 10.0},
                {-1.0, -1.0, 1.0},
                {0.0, 0.0, 0.0},
                {-10.0, 0.0, -0.0}
        };
    }

    @DataProvider(name = "Numbers of long type for div")
    public static Object[][] numbersLongTypeForDiv() {
        return new Object[][]{
                {0L, 1L, 0L},
                {-2L, 1L, -2L},
                {-2L, -1L, 2L},
                {6L, 2L, 3L},
                {1L, 0L, Exception.class}
        };
    }



    @DataProvider(name = "Numbers of long type for div by zero")
    public static Object[][] numbersLongTypeForDivByZero() {
        return new Object[][]{
                {1L, 0L}
        };
    }

    @DataProvider(name = "Numbers of double type for div")
    public static Object[][] numbersDoubleTypeForDiv() {
        return new Object[][]{
                {0.0, 1.0, 0.0},
                {1.0, 0.0, Exception.class},
                {-2.0, 1.1, -1.8},
                {2.0, -1.1, -1.8},
                {6.0, 3.0, 2.0}
        };
    }

    @DataProvider(name = "Numbers of double type for div by zero")
    public static Object[][] numbersDoubleTypeForDivByZero() {
        return new Object[][]{
                {1.0, 0.0}

        };
    }

    @DataProvider(name = "Numbers of double type for pow")
    public static Object[][] numbersDoubleTypeForPow() {
        return new Object[][]{
                {3.4, 2.2, 11.56},
                {2.0, 0.0, 1.0},
                {-2.0, 2.0, 4.0},
                {-2.0, 3.0, -8.0},
                {2.0, -1.0, 0.5},
                {-2.0, -1.0, -0.5}
        };
    }

    @DataProvider(name = "Numbers of double type for sqrt")
    public static Object[][] numbersDoubleTypeForSqrt() {
        return new Object[][]{
                {0.0, 0.0},
                {2.3, 1.516},
                {-2.3, 1.516}
        };
    }

    @DataProvider(name = "Numbers of double type for tg")
    public static Object[][] numbersDoubleTypeForTg() {
        return new Object[][]{
                {0.0, 0.0},
                {30.0, -6.405},
                {60.0, 0.320},
                {90.0, -1.995},
                {180.0, 1.339},
                {270.0, -0.179},
                {-30.0, 6.405},
                {-60.0, -0.320},
                {-90.0, 1.995},
                {-180.0, -1.339},
                {-270.0, 0.179}
        };
    }

    @DataProvider(name = "Numbers of double type for sin")
    public static Object[][] numbersDoubleTypeForSin() {
        return new Object[][]{
                {0.0, 0.0},
                {30.0, -0.988},
                {60.0, -0.305},
                {90.0, 0.894},
                {180.0, -0.801},
                {270.0, -0.176},
                {-30.0, 0.988},
                {-60.0, 0.305},
                {-90.0, -0.894},
                {-180.0, 0.801},
                {-270.0, 0.176}
        };
    }

    @DataProvider(name = "Numbers of double type for cos")
    public static Object[][] numbersDoubleTypeForCos() {
        return new Object[][]{
                {0.0, 1.0},
                {30.0, 0.154},
                {60.0, -0.952},
                {90.0, -0.448},
                {180.0, -0.598},
                {270.0, 0.984},
                {-30.0, 0.154},
                {-60.0, -0.952},
                {-90.0, -0.448},
                {-180.0, 0.598},
                {-270.0, 0.984}
        };
    }

    @DataProvider(name = "Numbers of double type for ctg")
    public static Object[][] numbersDoubleTypeForCtg() {
        return new Object[][]{
                {0.0, Exception.class},
                {30.0, -0.156},
                {60.0, 3.125},
                {90.0, -0.501},
                {180.0, 0.747},
                {270.0, -5.592},
                {-30.0, 0.156},
                {-60.0, -3.125},
                {-90.0, 0.501},
                {-180.0, -0.747},
                {-270.0, 5.592}
        };
    }

    @DataProvider(name = "Numbers of double type for 180 rad ctg")
    public static Object[][] numbersDoubleTypeFor180RadCtg() {
        return new Object[][]{
                {0.0}
        };
    }

    @DataProvider(name = "Numbers for check isPositive")
    public static Object[][] numbersIsPositive() {
        return new Object[][]{
                {30L, true},
                {-30L, false},
                {0L, false},
        };
    }

    @DataProvider(name = "Numbers for check isNegative")
    public static Object[][] numbersIsNegative() {
        return new Object[][]{
                {30L, false},
                {-30L, true},
                {0L, false},
        };
    }

}
