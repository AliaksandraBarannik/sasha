@ebay
Feature: Check the product in the basket

  Scenario:
    Given I opened Ebay home page
    And I searched the product "Samsung galaxy S6"
    And I clicked on the first product
    When I added the product to the basket and opened the basket
    Then I checked the product "Samsung Galaxy S6" was in the basket