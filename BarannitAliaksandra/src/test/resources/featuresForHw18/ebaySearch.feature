@ebay
Feature: Search product

  Scenario: Running a Full Text Quick Search
    Given I opened ebay HomePage to search the product
    When I searched product "Samsung galaxy S6"
    Then the product "Samsung galaxy S6" should be the first in the Search Result grid


  Scenario Outline: Running a Full Text Quick Search
    Given I opened ebay HomePage to search the product
    When I searched product "<request>"
    Then the product "<request>" should be the first in the Search Result grid

    Examples:
      | request |
      | Samsung galaxy S6 |
      | Lenovo ideapad 320 |


