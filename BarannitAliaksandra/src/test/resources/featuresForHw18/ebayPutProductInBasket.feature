@ebay
Feature: Check the product in the basket

  Scenario:
    Given I opened ebay HomePage to search the product
    When I searched product "Samsung galaxy S6"
    And I clicked on the first product
    And I added the product to the basket and opened the basket
    Then I checked the product "Samsung Galaxy S6" was in the basket