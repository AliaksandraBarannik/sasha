package com.tat.barannik;

import java.lang.Math.*;
public class Main {

    public static void main(String args[]) {
        int a;
        int p;
        double m1;
        double m2;
        int exp1 = 2;
        int exp2 = 3;
        double PI;
        double g;
        double sum;
        double denom;
        PI = java.lang.Math.PI;

        try {
            a = Integer.parseInt(args[0]);
            p = Integer.parseInt(args[1]);
            m1 = Double.parseDouble(args[2]);
            m2 = Double.parseDouble(args[3]);
            sum = m1 + m2;
            denom = Math.pow(p, exp1) * sum;
            if (denom != 0) {
                g = 4 * Math.pow(PI, exp1) * Math.pow(a, exp2) / denom;
                System.out.println(g);
            } else {
                throw new ArithmeticException("Division by zero is forbidden.");
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }
}
